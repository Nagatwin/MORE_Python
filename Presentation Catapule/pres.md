## Les catapultes peuvent-elles vraiment détruire Minas Tirith ?

Une question existentielle résolue scientifiquement


## A propos de la catapulte

## Qu'est ce qu'une catapulte
>La catapulte est une machine de guerre utilisée pour lancer des projectiles à grande distance, sans emploi d'aucun explosif.
> -- <cite>Wikipédia</cite>

Origine étymologique : du grec « kata » qui signifie
transpercer et « pelte » qui signifie bouclier.

## A quoi ça ressemble
![](catapulte.jpg)

## Comment ça fonctionne

On met du poids, et par effet de levier, le projectile est envoyé loin (très loin).

## Estimation de la portée de la catapulte

Partons du schéma suivant :
![](schema.jpg)


##

Nous savons tous que, en négligeant les frottements, la portée s'exprime :

$$d=\frac{v_0\,\cos(\theta)}{g}\left(v_0\,\sin(\theta) + \sqrt{\left(v_0\,\sin{\theta}\right)^{2} + 2\,g\,y_0}\right)$$

Et la hauteur maximale

$$\frac{v_0^2\,\sin^2(\theta)}{2\,g}$$

## Estimation des paramètres dans le cas de la bataille de Minas Tirith


## Distance

##
![](distance1.jpg)

##
![](distance2.jpg)

##
![](distance3.jpg)

##
![](distance4.jpg)

##
![](distance5.jpg)

##
![](distance6.jpg)

##
![](distance7.jpg)

## Angle

##
![](distance1.jpg)

##
![](distance8.jpg)

##
![](distance9.jpg)

##
![](distance10.jpg)

##
![](distance11.jpg)

##
![](distance12.jpg)

##
Les paramètres sont donc :

$$y_{0}=6m,$$$$d=33m$$ et $$\theta = 35°$$

##
On calcule les valeurs par un script Python :
```python
from math import sqrt, sin, cos, pi

def preuve_que_Sauron_peut_pas_gagner(theta, v0, y0):
    g = 9.81
	square = (v0*sin(theta))**2
	temp = v0*sin(theta) + sqrt((square+2*g*y0)
	d = v0*cos(theta)*temp/g
	h = square/(2*g)
	return h, d

for i in range(1,5):
    print(preuve_que_Sauron_peut_pas_gagner((35*pi/180), 10*i, 6))
```

##

| $$v_{0}$$     |  Hauteur max  | Portée      |
|:-------------:|:-------------:|:-----------:|
| 10 m/s        |  10.79 m      |  11.57 m    |
| 20 m/s        |  12.71 m      |  48.31 m    |
| 30 m/s        |  21.10 m      |  86.21 m    |
| 40 m/s        |  32.83 m      |  153.26 m   |

## Conclusion
Il faut que la vitesse initiale soit supérieure à 20m/s pour pouvoir
espérer détruire Minas Tirith.
