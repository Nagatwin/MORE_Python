# Plan de Test

## Introduction

Ce plan de test est destiné à assurer l'intégrité de l'application MORE.

Les tests seront de deux types : **manuels** et **automatiques**.
Les tests automatiques seront gérés par GitLab à chaque push.
Quant aux tests manuels, ils sont laissés à la charge de ceux voulant réaliser le push.


## Fonctionnalités testées

* [Upload de fichiers](#upload-de-fichiers)
  * [Drag and drop](#drag-and-drop)
  * [Listing des fichiers](#listing-des-fichiers)
  * [Envoi de fichiers](#envoi-de-fichiers)
  * [Création de la hiérarchie](#création-de-la-hiérarchie)
* [Conversion en fichier MORE](#conversion-en-fichier-more)
    * [Edition des chemins](#edition-des-chemins)
* [Communication avec le serveur](#communication-avec-le-serveur)
    * [Lancement du serveur](#lancement-du-serveur)
    * [Envoi de requêtes](#envoi-de-requêtes)
    * [Authentification des clients](#authentification-des-clients)
    * [Droits administrateur](#droits-administrateur)
* [Affichage de la présentation](#affichage-de-la-présentation)
    * [Formattage du texte](#formattage-du-texte)
    * [Présence de tout le contenu](#présence-de-tout-le-contenu)
    * [Affichage des images](#affichage-des-images)
    * [Affichage des vidéos](#affichage-des-vicéos)
    * [Les transitions sont fluides](#les-transitions-sont-fluides)
    * [Le LaTeX est compilé](#le-latex-est-compilé)
    * [Coloration syntaxique](#coloration-syntaxique)
    * [Icône de paramètres](#icône-de-paramètres)
    * [Pointeurs](#pointeurs)
* [Synchronisation](#synchronisation)
    * [Le master pilote tout le monde](#le-master-pilote-tout-le-monde)
    * [Les clients peuvent se désynchroniser](#les-clients-peuvent-se-désynchroniser)
    * [Resynchronisation du client](#resynchronisation-du-client)
        * [Renvoi du slideshow](#renvoi-du-slideshow)
        * [Renvoi de la slide](#renvoi-de-la-slide)
    * [Index max](#index-max)
        * [Limitation du client](#limitation-du-client)
        * [Mise à jour du max en synchro](#mise-à-jour-du-max)
        * [Mise à jour du max hors synchro](#mise-à-jour-du-max-hors-synchro)
        * [Sauvegarde du max](#sauvegarde-du-max)
    * [Switch de présentation](#switch-de-présentation)
    * [Synchronisation vidéo](#synchronisatio-vidéo)
    * [Autorisation du pointage](#autorisation-du-pointage)
* [Interface administrateur](#interface-administrateur)
    * [Affichage du menu](#affichage-du-menu)
    * [Création des sessions](#création-des-sessions)
    * [Listing des sessions actives](#listing-des-sessions-actives)
    * [Suppression automatique des sessions vides](#suppression-automatique-des-sessions-vides)
    * [Gestion des formulaires](#gestion-des-formulaires)
        * [Sélection-multiple](#sélection-multiple)
        * [Obligation du nom](#obligation-du-nom)



## Fonctionnalités non testées
* Upload de fichiers
    * Intégrité des fichiers de sortie

* Conversion en fichier MORE
    * La qualité des présentations en sortie



# Description des tests

## Upload de fichiers

#### Drag and drop
>**Type de test** : Manuel

>**But du test** : S'assurer que le drag and drop fonctionne correctement.

#### Listing des fichiers
>**Type de test** : Manuel

>**But du test** : S'assurer que les noms des fichiers sont bien écrits dans les text area.

#### Envoi de fichiers
>**Type de test** : Automatique

>**Fichier de test** : non-défini

>**Entrée** : Une requête d'envoi de fichiers

>**Sortie** : La réponse du serveur (confirmation)

#### Création de la hiérarchie
>**Type de test** : Manuel

>**But du test** : Vérifier qu'après upload sont crées : le dossier portant le nom de la présentation, et le dossier content


## Conversion en fichier MORE

La fonctionnalité est implémentée dans le serveur, on ne réalisera qu'un test End-to-End.

>**Type de test** : Automatique

>**Fichier de test** : non-défini

>**Entrée** : Une liste de fichiers d'entrée, selon plusieurs formats différents

>**Sortie** : La liste des fichiers 

#### Edition des chemins

>**Type de test** : Manuel

>**But du test** : Nous traitons le fichier MORE après pandoc, pour modifier les chemins d'accès des fichiers "contents". Il faut vérifier que ces chemins sont correctement édités.

## Communication avec le serveur


#### Lancement du serveur

>**Type de test** : Automatique

>**Fichier de test** : non-défini

>**Entrée** : Requête de lancement

>**Sortie** : Confirmation du bon lancement du serveur

#### Envoi de requêtes
>**Type de test** : Automatique

>**Fichier de test** : non-défini

>**Entrée** : Chaque requête possible

>**Sortie** : Le message du serveur suite au traitement de la requête

#### Authentification des clients
>**Type de test** : Automatique

>**Fichier de test** : non-défini

>**Entrée** : la connexion d'un nouveau client

>**Sortie** : l'id du client

#### Droits administrateur
>**Type de test** : Manuel

>**But du test** : S'assurer que les droits d'administrateur sont bien gérés (upgrade des clients)

## Affichage de la présentation


#### Formattage du texte
>**Type de test** : Manuel

>**But du test** : S'assurer qu'il n'y a pas de problème dans l'encodage, le placement du texte

#### Présence de tout le contenu
>**Type de test** : Manuel

>**But du test** : S'assurer que tout le contenu de la présentation est bien présent (images, graphes, codes, textes)

#### Affichage des images]
>**Type de test** : Manuel

>**But du test** : Vérifier l'affichage des images dans la présentation.


#### Affichage des vidéos
>**Type de test** : Manuel

>**But du test** : Vérifier que les vidéos sont bien lues et bien affichées.


#### Les transitions sont fluides
>**Type de test** : Manuel

>**But du test** : S'assurer de la fluidité des transitions

#### Le LaTeX est compilé
>**Type de test** : Manuel

>**But du test** : Vérifier que le LaTeX est compilé dans la présentation.


#### Coloration syntaxique
>**Type de test** : Manuel

>**But du test** : Visualiser la coloration syntaxique des codes sources.


#### Icône de paramètres
>**Type de test** : Manuel

>**But du test** : Vérifier que l'icône de paramètres est bien là


#### Pointeurs
>**Type de test** : Manuel

>**But du test** : Visualiser les pointeurs sur l'écran


## Synchronisation

#### Le master pilote tout le monde
>**Description du test** : Le master doit pouvoir piloter tous les écrans connectés sur la présentation, si bien à l'aide des flèches de reveal.js qu'avec les touches directionnelles (ou swipe pour les téléphones)


#### Les clients peuvent se désynchroniser
>**Description du test** : Les clients ont la possibilité de se désynchroniser du master pour se déplacer librement dans les slides déjà vues.
Une fois entré dans le mode non-synchronisé, la modification faite par le master ne doit pas impacter la navigation du client.

#### Resynchronisation du client
>**Description du test** : Le client peut se resynchroniser avec le master.

###### Renvoi du slideshow
>** Description du test** : Lorsque le client se resynchronise, le serveur doit lui renvoyer la présentation en cours (au cas où elle aurait changé)

###### Renvoi de la slide
>**Description du test** : Lorsque le client se resynchronise, le serveur doit lui renvoyer la slide en cours.

#### Index max
>**Description** : Pour éviter que le client ait accès à toute la présentation, nous avons mis en place un index qui indique la progression maximale de la présentation.

###### Limitation du client
>**Description du test** : Il faut vérifier que le client ne puisse pas être en avance sur le master lors de la présentation.

###### Mise à jour du max
>**Description du test** : Lors de chaque modification du master, l'index max doit être mis à jour (soit augmenté, soit maintenu). Le client doit pouvoir alors aller jusqu'à ce nouvel index max.

###### Mise à jour du max hors synchro
>**Description du test** : La modification du max doit être manifeste aussi pour les clients non synchronisés avec la présentation du master.

###### Sauvegarde du max
>**Description du test** : L'index max doit être sauvegardé en mémoire lors du changement de présentation.


#### Switch de présentation
>**Description du test** : Le master doit pouvoir changer de présentation, et l'avancement de chaque présentation doit être gardé en mémoire.

#### Synchronisation vidéo
>**Description du test** : La présentation doit pouvoir être pilotée par une vidéo.

#### Autorisation du pointage
>**Description du test** : Le master doit pouvoir autoriser des clients à utiliser le pointeur.


## Interface administrateur
>**Description** : Tous ces tests portent sur l'interface web administrateur, à partir de laquelle sont gérées l'upload et les sessions

#### Affichage du menu
>**Description du test** : S'assurer que le menu est correctement affiché.

#### Création des sessions
>**Description du test** : L'administrateur doit pouvoir créer des sessions de présentation.

#### Listing des sessions actives
>**Description du test** : Les sessions actives sont correctement listées sur la page d'accueil, avec des liens fonctionnels.

#### Suppression automatique des sessions vides
>**Description du test** : Les sessions vides doivent être automatiquement supprimées (SAUF en cas de refresh de la page).

#### Gestion des formulaires
>**Description** : S'assurer du bon déroulement des formulaires.

###### Sélection-multiple
>**Description du test** : On peut sélectionner de multiples présentations.

###### Obligation du nom
>**Description du test** : Vérifier que le formulaire ne peut pas être validé sans que l'administrateur ait soumis un nom.

