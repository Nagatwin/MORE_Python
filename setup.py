from setuptools import setup

setup(
    name='MORE',
    version='1.0.0',
    author='Edward Tombre, Guillaume Goyon, Tristan Nemoz',
    author_email='edward.tombre@telecom-paristech.fr',
    install_requires=['bottle-websocket', 'bottle'],
)
