# Projet MORE

[POSTER DE PRESENTATION](./Poster/unNomTresClairPourUneNouvelleVieSansEspaceEtSansAccent.pdf)
![screen](MORE.png)

MORE est un système de présentation multi-écrans synchronisés pour divers types de contenus simples : PowerPoints ou vidéos. Un serveur sert les fichiers nécessaires aux présentations et gère la synchronisation entre les divers écrans.

Il a été réalisé dans le cadre du Projet d'Application Final (PAF) 2018 à Télécom Paristech.

C'est un projet encadré par **Jean-Claude Dufour** .

Il a été réalisé par :

* **Guillaume Goyon**
* **Tristan Nemoz**
* **Edward Tombre**

## Scénario cœur du projet, donné par un professeur

>Je donne un cours en B551. J’ai mon Mac et mon téléphone. Les transparents ont été faits en markdown et traduits en HTML5+CSS+JS. Pendant le cours, je marche dans toute la classe et je me sers de mon téléphone pour faire avancer les transparents et pour pointer. Il y a une seconde présentation avec juste mes notes, qui est sur une fenêtre non projetée sur mon écran. Cette seconde présentation est synchronisée avec les transparents projetés.
Certain.e.s étudiant.e.s ont leur ordi avec eux et regardent les transparents sur leur écran. Leur présentation est synchronisée avec la mienne, ou pas, à leur choix.
J’ai une vidéo d’un professeur qui était en séjour sabbatique chez nous l’an dernier. Je démarre cette vidéo dont le temps pilote mes transparents.
Quand je pause la vidéo pour faire un commentaire puis redémarre, les transparents restent synchronisés avec la vidéo.
Les élèves, sur leur ordi, peuvent toujours voir les transparents et/ou la vidéo, par défaut synchronisés.
Une question est posée sur un transparent précédent. En revenant sur ce transparent, la vidéo est automatiquement positionnée sur le temps correspondant au transparent.

---
## Comment l'utiliser
### Prérequis
* Installation de [Pandoc](https://pandoc.org/) pour convertir les présentations. Il est nécessaire de le placer dans le PATH.
* Installation de la bibliothèque Python [Bottle-websocket](https://github.com/zeekay/bottle-websocket) qui est le cœur du serveur python.

### Installation de Bottle-Websocket
Exécuter le fichier d'installation :
```bash
python setup.py install
```
Lancer le serveur python [server.py](Bottle/server.py) grâce au runner [run.bat](Bottle/run.bat).
Ouvrir un navigateur et se connecter aux URLs générées par le serveur selon le rôle voulu.

### Utilisation

- Lancer le runner : run.bat
- Configurer le serveur selon ses indications, en choisissant notamment son adresse IP et son port de connexion.
- Ouvrir un navigateur autre que Safari et se connecter sur l'adresse IP indiquée au port indiqué.

#### Upload des présentations

1. Cliquer sur l'onglet **Upload** à gauche de l'interface.
2. Dans la page qui s'affiche, glisser-déposer dans la zone de gauche les fichiers de PowerPoints (au format .md par exemple), les fichiers vidéo (format .mp4, .ogg ou .webm) et les fichiers MOREVIDEO.
3. Dans la zone de droite, glisser-déposer tout autre contenu servant à la présentation, tel que des images ou les styles personnalisés. Il n'y a aucun problème à upload un contenu servant à plusieurs Powerpoints.
4. Finaliser en cliquant sur le bouton associé.

##### Le fichier .morevideo
C’est un fichier avec les timestamp en secondes reliés aux slides (slideX;slideY)
Par exemple :
```
0 0;0
0.2 0;1
0.5 0;2
3 1;2
5 1;4
10 0;0
11 5;3
...
```

#### Lancer une session

1. Cliquer sur l'onglet **Sessions** si vous étiez dans l'onglet **Upload**.
2. Cliquer sur "Create session"
3. Sélectionner les présentations désirées
4. Les liens Viewers et Masters sont ensuite affichés, ainsi que le QR code de master, vous pouvez alors les partager
5. Dans l'onglet sessions, vous aurez une liste de sessions actives
6. La session sera automatiquement supprimée si personne n'est dedans

#### Les Viewers

Les Viewers ont lors d'une présentation la possibilité de se désynchroniser ou de demander le pointeur via l'option paramètres en haut à gauche de la fenêtre.

#### Les Masters

Les Masters ont plusieurs options dans le menu paramètres en haut à gauche de la fenêtre:
* choisir ce qui est affiché sur leur écran et sur celui des Viewers
* choisir la présentation courante
* choisir s’ils sont synchronisés ou non
* accepter ou non les demandes de pointeur

Ils peuvent aussi pointer avec leur curseur (clic sur l'écran)

## Ensemble des fonctionnalités
#### Mise en forme des présentations
- [x] Conversion de la présentation.
- [x] Gestion des images.
- [x] Gestion des vidéos.
- [x] Coloration syntaxique de code source.
- [x] Affichage de mathématiques avec MathJax.

#### Communication serveur
- [x] Envoi sur le serveur.
- [x] Récupération depuis le serveur.
- [x] Gestion des droits.

#### Synchronisation
- [x] Synchronisation de l'affichage multi-écrans.
- [x] Synchronisation de présentations différentes.
- [x] Synchronisation depuis un téléphone.
- [x] Synchronisation depuis une vidéo.
- [x] Gestion des pointeurs.
- [x] Gestion de plusieurs sessions.
- [x] Gestion de plusieurs présentations sur une même session.
- [x] Possibilité pour un master de se désynchroniser.

#### Interface
- [x] Interface d'upload.
- [x] Interface de gestion des sessions.
- [x] Garbage collector des sessions.
- [x] Possibilité de créer une session Viewer par flash d'un QR code.

---
## Dossiers

* [Bottle](Bottle) : Le serveur python et tous les fichiers nécessaires à son fonctionnement + tests
* [Demo](Demo) : Présentation(s) utilisées lors de la démo du PAF
* [Poster](Poster) : Poster utilisé lors de la présentation du PAF
* [Testing](Testing) : La liste des tests à effectuer d'une version à l'autre

---
