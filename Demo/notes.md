## MORE Python Notes
Là on présente le projet

## Synchronisation de powerpoints Notes
Là on parle de :

* Multi-écrans
* Multi-ordinateurs
* Multi-powerpoints
* Des droits différents

## Notes Notes
Et là on dit que on a nos notes synchronisées avec le reste :

* Notes : Possibilité de mettre des notes synchronisées avec le reste des powerpoints.. ou pas.

## Vidéos Notes
* Vidéos : La vidéo pilote la présentation; Le timer de la vidéo est synchronisé sur l'index du powerpoint

## Pointeur Notes
* Pour le master : Un pointeur permet de montrer des détails
* Pour les viewers : Les viewers peuvent demander au master la permission d'acceder au pointeur pour montrer des détails

## Upload Notes
* Formats d'entrée : .tex, .md, .adoc, .epub, .html ...
* CSS custom : Possibilité d'utiliser son propre style pour les présentations
* .morevideo

## QR code Notes
* QR code : La génération automatique de QR code pour rejoindre les sessions.

## Support du LaTeX Notes
* LaTeX : Support de beamer au format .tex; Formules intégrées;  FORMULE COMPLIQUEE

## Multi-présentation Notes
* Multi-présentation : Plusieurs présentations désynchronisées entre elles; Passer de l'une à l'autre sans problème

## Multi-session Notes 
* Multi-sessions : Gestion de plusieurs sessions indépendantes; Panel de liste des sessions; Nettoyage automatique des sessions terminées

## Technologies utilisées Notes
* Technologies utilisées : HTML; CSS; JS (Websocket); Python 2/3 (Bottle, Websocket); Pandoc; Reveal.js; LaTeX