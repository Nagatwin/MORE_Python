## MORE Python

## Synchronisation de powerpoints
### Multi-écrans
Les powerpoints sont synchronisés sur plusieurs écrans reliés au même ordinateur.

### Multi-ordinateurs
Les powerpoints sont synchronisés sur plusieurs ordinateurs connectés au serveur.

### Multi-powerpoints

Une présentation permet de gérer, afficher et synchroniser plusieurs powerpoint.

On peut changer l'affichage des viewers et de chaque master indépendemment.

### Des droits différents
* Les masters définissent la slide max
* Les viewers ne peuvent pas dépasser la slide max

## Notes
### Notes
Possibilité de mettre des notes synchronisées avec le reste des powerpoints.. ou pas.

### Coloration
Coloration syntaxique efficace
```python
class Client:
    """Class for users."""
    def __init__(self, session_id, socket):
        self.session = session_id
        self.socket = socket

    def send(self, message):
        self.socket.send(message)
```

## Vidéos
### Vidéos
* La vidéo pilote la présentation.
* Le timer de la vidéo est synchronisé sur l'index du powerpoint

## Pointeur
### Pour le master
Un pointeur permet de montrer des détails

### Pour les viewers
Les viewers peuvent demander au master la permission d'acceder au pointeur pour montrer des détails

## Upload de présentations
### Formats d'entrée
.tex, .md, .adoc, .epub, .html ...

### CSS custom
Possibilité d'utiliser son propre style pour les présentations

### Timers de vidéo
Fichier au format .morevideo : 

time slideX;slideY

## QR code
### QR code
La génération automatique de QR code pour rejoindre les sessions.

## Support du LaTeX
### LaTeX
* Support de beamer au format .tex
* Formules intégrées
$$\mathcal{F}(s):\nu\mapsto\int_{-\infty}^{+\infty}s(t)\,\mathrm{e}^{-2\,\mathrm{i}\,\pi\,\nu\,t}\,\mathrm{d}t$$

## Multi-présentation
### Multi-présentation
* Plusieurs présentations désynchronisées entre elles.
* Passer de l'une à l'autre sans problème

## Multi-session
### Multi-sessions
* Gestion de plusieurs sessions indépendantes
* Panel de liste des sessions
* Nettoyage automatique des sessions terminées

## Technologies utilisées
### Technologies utilisées
* HTML
* CSS
* JS (Websocket)
* Python 2/3 (Bottle, Websocket)
* Pandoc
* Reveal.js (highlight.js, ..)
* LaTeX