# Compares two indexes, given as list (after split)

from converter import to_int


def max_indexes_as_lists(first_list, second_list):
    # If they are not equal, return the highest
    if to_int(first_list[0]) > to_int(second_list[0]):
        return first_list
    elif to_int(first_list[0]) < to_int(second_list[0]):
        return second_list
    # Else, test if we have reached the last element of comparison
    elif len(first_list) == 1:
        return first_list
    # Else, compare the next stage of index
    return [first_list[0]] + max_indexes_as_lists(first_list[1:], second_list[1:])


# Compares two indexes by comparing their list transformation
def get_max_index(first_index, second_index):
    first_list = first_index.split(";")
    second_list = second_index.split(";")
    if max_indexes_as_lists(first_list, second_list) == first_list:
        return first_index
    return second_index
