# Presentations
Ce dossier contient les fichiers relatifs aux présentations, que ce soit leur design ou leur contenu.

Chaque présentation est construite de la manière suivante :
* Un fichier MORE : il est le résultat de la conversion du fichier MarkDown. Il contient les infos utiles au serveur pour qu'il affiche correctement la présentation.
* Éventuellement un fichier MOREVIDEO et un fichier vidéo si la présentation est destinée à être synchronisée avec ledit fichier vidéo.
* Un dossier contents : Il contient tous les autres contenus relatifs à la présentation, comme les images par exemple.

---

## Notes

### Les formats de fichiers
Nous avons créé pour ce projet quelques formats personnalisés :
- les fichiers .more : ils contiennent un code HTML représentant la présentation. Il se situe à la racine du dossier d'une présentation.
- les fichiers .morevideo : ils contiennent du texte représentant la synchronisation d'une vidéo avec une présentation. Il se situe à la racine d'une présentation.

### La gestion des contenus
Tous les contenus d'une présentation sont dans le dossier contents à l'intérieur du dossier d'une présentation. L'utilisateur peut définir le chemin qu'il veut pour ses contenus dans sa présentation, le serveur convertira les chemins d'accès lors de la conversion en fichier .more.