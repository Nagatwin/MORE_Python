<section id="les-catapultes-peuvent-elles-vraiment-détruire-minas-tirith" class="slide level2">
<h2>Les catapultes peuvent-elles vraiment détruire Minas Tirith ?</h2>
<p>Une question existentielle résolue scientifiquement</p>
</section>
<section id="a-propos-de-la-catapulte" class="slide level2">
<h2>A propos de la catapulte</h2>
</section>
<section id="quest-ce-quune-catapulte" class="slide level2">
<h2>Qu’est ce qu’une catapulte</h2>
<blockquote>
<p>La catapulte est une machine de guerre utilisée pour lancer des projectiles à grande distance, sans emploi d’aucun explosif. – <cite>Wikipédia</cite></p>
</blockquote>
<p>Origine étymologique : du grec « kata » qui signifie transpercer et « pelte » qui signifie bouclier.</p>
</section>
<section id="a-quoi-ça-ressemble" class="slide level2">
<h2>A quoi ça ressemble</h2>
<p><img data-src="Catapulte-probleme/contents/catapulte.jpg" /></p>
</section>
<section id="comment-ça-fonctionne" class="slide level2">
<h2>Comment ça fonctionne</h2>
<p>On met du poids, et par effet de levier, le projectile est envoyé loin (très loin).</p>
</section>
<section id="estimation-de-la-portée-de-la-catapulte" class="slide level2">
<h2>Estimation de la portée de la catapulte</h2>
<p>Partons du schéma suivant : <img data-src="Catapulte-probleme/contents/schema.jpg" /></p>
</section>
<section id="section" class="slide level2">
<h2></h2>
<p>Nous savons tous que, en négligeant les frottements, la portée s’exprime :</p>
<p><span class="math display">\[d=\frac{v_0\,\cos(\theta)}{g}\left(v_0\,\sin(\theta) + \sqrt{\left(v_0\,\sin{\theta}\right)^{2} + 2\,g\,y_0}\right)\]</span></p>
<p>Et la hauteur maximale</p>
<p><span class="math display">\[\frac{v_0^2\,\sin^2(\theta)}{2\,g}\]</span></p>
</section>
<section id="estimation-des-paramètres-dans-le-cas-de-la-bataille-de-minas-tirith" class="slide level2">
<h2>Estimation des paramètres dans le cas de la bataille de Minas Tirith</h2>
</section>
<section id="distance" class="slide level2">
<h2>Distance</h2>
</section>
<section id="section-1" class="slide level2">
<h2></h2>
<p><img data-src="Catapulte-probleme/contents/distance1.jpg" /></p>
</section>
<section id="section-2" class="slide level2">
<h2></h2>
<p><img data-src="Catapulte-probleme/contents/distance2.jpg" /></p>
</section>
<section id="section-3" class="slide level2">
<h2></h2>
<p><img data-src="Catapulte-probleme/contents/distance3.jpg" /></p>
</section>
<section id="section-4" class="slide level2">
<h2></h2>
<p><img data-src="Catapulte-probleme/contents/distance4.jpg" /></p>
</section>
<section id="section-5" class="slide level2">
<h2></h2>
<p><img data-src="Catapulte-probleme/contents/distance5.jpg" /></p>
</section>
<section id="section-6" class="slide level2">
<h2></h2>
<p><img data-src="Catapulte-probleme/contents/distance6.jpg" /></p>
</section>
<section id="section-7" class="slide level2">
<h2></h2>
<p><img data-src="Catapulte-probleme/contents/distance7.jpg" /></p>
</section>
<section id="angle" class="slide level2">
<h2>Angle</h2>
</section>
<section id="section-8" class="slide level2">
<h2></h2>
<p><img data-src="Catapulte-probleme/contents/distance1.jpg" /></p>
</section>
<section id="section-9" class="slide level2">
<h2></h2>
<p><img data-src="Catapulte-probleme/contents/distance8.jpg" /></p>
</section>
<section id="section-10" class="slide level2">
<h2></h2>
<p><img data-src="Catapulte-probleme/contents/distance9.jpg" /></p>
</section>
<section id="section-11" class="slide level2">
<h2></h2>
<p><img data-src="Catapulte-probleme/contents/distance10.jpg" /></p>
</section>
<section id="section-12" class="slide level2">
<h2></h2>
<p><img data-src="Catapulte-probleme/contents/distance11.jpg" /></p>
</section>
<section id="section-13" class="slide level2">
<h2></h2>
<p><img data-src="Catapulte-probleme/contents/distance12.jpg" /></p>
</section>
<section id="section-14" class="slide level2">
<h2></h2>
<p>Les paramètres sont donc :</p>
<p><span class="math display">\[y_{0}=6m,\]</span><span class="math display">\[d=33m\]</span> et <span class="math display">\[\theta = 35°\]</span></p>
</section>
<section id="section-15" class="slide level2">
<h2></h2>
<p>On calcule les valeurs par un script Python :</p>
<pre class="python"><code>from math import sqrt, sin, cos, pi

def preuve_que_Sauron_peut_pas_gagner(theta, v0, y0):
    g = 9.81
    square = (v0*sin(theta))**2
    temp = v0*sin(theta) + sqrt((square+2*g*y0)
    d = v0*cos(theta)*temp/g
    h = square/(2*g)
    return h, d

for i in range(1,5):
    print(preuve_que_Sauron_peut_pas_gagner((35*pi/180), 10*i, 6))</code></pre>
</section>
<section id="section-16" class="slide level2">
<h2></h2>
<table>
<thead>
<tr class="header">
<th style="text-align: center;"><span class="math display">\[v_{0}\]</span></th>
<th style="text-align: center;">Hauteur max</th>
<th style="text-align: center;">Portée</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: center;">10 m/s</td>
<td style="text-align: center;">10.79 m</td>
<td style="text-align: center;">11.57 m</td>
</tr>
<tr class="even">
<td style="text-align: center;">20 m/s</td>
<td style="text-align: center;">12.71 m</td>
<td style="text-align: center;">48.31 m</td>
</tr>
<tr class="odd">
<td style="text-align: center;">30 m/s</td>
<td style="text-align: center;">21.10 m</td>
<td style="text-align: center;">86.21 m</td>
</tr>
<tr class="even">
<td style="text-align: center;">40 m/s</td>
<td style="text-align: center;">32.83 m</td>
<td style="text-align: center;">153.26 m</td>
</tr>
</tbody>
</table>
</section>
<section id="conclusion" class="slide level2">
<h2>Conclusion</h2>
<p>Il faut que la vitesse initiale soit supérieure à 20m/s pour pouvoir espérer détruire Minas Tirith.</p>
</section>
