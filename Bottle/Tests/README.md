# Tests

Ce dossier contient toutes les présentations servant à réaliser les tests des différentes fonctionnalités. Tous peuvent servir à tester la synchronisation et l'interface, mais chaque présentation possède ses spécificités.

## Description détailleé
* [test-code-source-court](Bottle/Presentations/test-code-source-court) : Présentation avec des codes sources courts en Python et en LaTeX. Il sert à tester le plugin highlight.js.
* [test-code-source-long](Bottle/Presentations/test-code-source-long) : Présentation avec des codes sources longs en ligne et en nombre de lignes en Python et en LaTeX. Il sert à tester des cas limites du plugin highlight.js.
* [test-custom-css](Bottle/Presentations/test-custom-css) : Présentation avec une feuille de stylle CSS différente de celle par défaut.
* [test-image](Bottle/Presentations/test-image) : Présentation avec une image. Il sert à tester l'affichage d'une image.
* [test-latex-slideshow](Bottle/Presentations/test-latex-slideshow) : Présentation créée à partir d'une présentation beamer donnée au format .tex.
* [test-maths](Bottle/Presentations/test-maths) : Présentation avec des formules mathématiques courtes, grandes et longues. Il sert à tester le plugin math.js dans des cas d'utilisations classiques et limites.
* [test-multi-slideshow](Bottle/Presentations/test-multi-slideshow) : Présentation avec slideshows multiples. Il sert à tester la multi-présentation.
* [test-video-1](Bottle/Presentations/test-video-1) : Présentation synchronisée avec une vidéo. Il sert à tester la synchronisation vidéo.

---