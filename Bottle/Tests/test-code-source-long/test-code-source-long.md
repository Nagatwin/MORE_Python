---
author: MORE Serveur Python
title: Présentation test de code source long
date: June 28, 2017
---
# Test de code source python
```python
class Session:

    """
    One set of user synchronized
    """

    def __init__(self,masterURI, viewerURI):
        self.viewers = []
        self.masters = []
        self.presentations = []
        self.displayed = None
        self.masterHashes = set()
        self.viewerHashes = set()
        self.masterURI=masterURI
        self.viewerURI=viewerURI

    def add_master(self, client):
        self.masters += [client]

    def add_viewer(self, client):
        self.viewers += [client]

    def remove_user(self, client_to_remove):
        self.masters = [client for client in self.masters if client != client_to_remove]
        self.viewers = [client for client in self.viewers if client != client_to_remove]

    def broadcast(self, message):
        self.send_viewers(message)
        self.send_masters(message)

    def send_viewers(self, message):
        for user in self.viewers:
            user.send(message)

    def send_masters(self, message):
        for user in self.masters:
            user.send(message)

    def upgrade_user(self, client):
        self.viewers.remove(client)
        self.masters += [client]

    def add_presentation(self, presentation):
        self.presentations += [presentation]
```
---
# Test de code source TeX
```latex
\documentclass[a4paper, 11pt, twocolumn, landscape]{report}

\usepackage[french]{babel}
\usepackage[T1]{fontenc}
\usepackage[latin1]{inputenc}

\usepackage{amssymb}
\usepackage{amsthm, thmtools}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{mathrsfs}
\usepackage{mathtools}
\usepackage{stmaryrd}

\usepackage{enumitem}

\usepackage{nameref, hyperref, cleveref}
	\hypersetup{
		pdfnewwindow=false,
		colorlinks=true,
		linkcolor=black,
		filecolor=black,
		urlcolor=blue,
		}

\usepackage{multicol}
	\setlength{\columnseprule}{0.1pt}

\usepackage[top=2cm, bottom=2cm, left=1.5cm, right=1.5cm]{geometry}

\usepackage{titletoc}
\usepackage{titlesec}
	\titleformat{name = \chapter, numberless}[display]{\normalfont\Large\filcenter}{\vspace{-1cm}\titlerule[1pt]\vspace{1pt}\titlerule}{-0.5pc}{\LARGE\filcenter}[\vspace{1pc}\titlerule]
	\titleformat{name = \chapter}[display]{\normalfont\Large\filcenter}{\vspace{-0.5cm}\titlerule[1pt]\vspace{1pt}\titlerule}{-0.5pc}{\Large\filcenter}[\vspace{0.5pc}\titlerule]
	\titleformat{name = \section, numberless}[display]{\normalfont\Large\filcenter}{\vspace{-0.5cm}\titlerule[1pt]\vspace{1pt}\titlerule}{-0.5pc}{\Large\filcenter}[\vspace{0.5pc}\titlerule]
	\titlespacing{\chapter}{0pt}{0pt}{*5}

\allowdisplaybreaks
\jot=0pt

\declaretheorem[numbered = no, name = Définition, refname = {définition, définitions}, Refname = {Définition, Définitions}]{definition}
\declaretheorem[numbered = no, name = Lemme, refname = {lemme, lemmes}, Refname = {Lemme, Lemmes}]{lemma}
\declaretheorem[numbered = no, name = Théorème, refname = {théorème, théorèmes}, Refname = {Théorème, Théorèmes}]{theorem}
\declaretheorem[numbered = no, name = Proposition, refname = {proposition, propositions}, Refname = {Proposition, Propositions}]{proposition}
\declaretheorem[numbered = no, name = Corollaire, refname = {corollaire, corollaires}, Refname = {Corollaire, Corollaires}]{corollary}

\begin{document}
	\tableofcontents
	\section*{Conventions, notations et rappels}
	\addcontentsline{toc}{chapter}{Conventions, notations et rappels}
	\begin{theorem}
		Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vulputate, est a volutpat congue, sem dolor eleifend nibh, ut porta dui augue et elit. Proin sit amet turpis tellus. Cras ut accumsan eros. Nulla vehicula fermentum laoreet. Vivamus vitae blandit tellus. Phasellus vehicula sollicitudin est, in fringilla odio tempor a. Suspendisse pretium lobortis feugiat.
	\end{theorem}
\end{document}
```