---
author: MORE Serveur Python
title: Présentation test de maths
date: June 28, 2017
---
# Formule classique
$$\mathcal{F}(s):\nu\mapsto\int_{-\infty}^{+\infty}s(t)\,\mathrm{e}^{-2\,\mathrm{i}\,\pi\,\nu\,t}\,\mathrm{d}t$$
---
# Formule grande
$$\frac{\sum\limits_{n=1}^{+\infty}\frac{1}{n^2}}{\int\limits_{0}^{\pi}x\,\mathrm{d}x}=\frac13$$
---
# Formule longue
$$\left(\frac{\Gamma'}{\Gamma}\right)^{(n)}(1)=\sum_{k=0}^n\sum_{r=1}^k\sum_{\substack{i_1\leqslant\cdots\leqslant i_r\\i_1+\cdots+i_r=k}}\begin{pmatrix}n\\k\end{pmatrix}\,k!\,(-1)^r\,r!\,\prod_{l=1}^r\frac{1}{i_j!}\,\prod_{m=1}^n\frac{1}{j_m!}\,\prod_{j=1}^r\Gamma^{\left(i_l\right)}(1)=(-1)^n\,(n-1)!\,\zeta(n)$$
---
# Maths in-line
Il est de notoriété publique que $\gamma\in\mathbf{R}\backslash\mathbf{Q}$, pourquoi un tel déni général ?