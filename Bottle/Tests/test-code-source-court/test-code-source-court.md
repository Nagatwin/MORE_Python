---
author: MORE Serveur Python
title: Présentation test de code source court
date: June 28, 2017
---
# Test de code source python
```python
class Client:
    """Class for users."""

    identifier = 0

    def __init__(self, session_id, socket):
        self.session = session_id
        self.socket = socket
        self.id = Client.identifier
        Client.identifier += 1
        self.__can_point = False

    def send(self, message):
        self.socket.send(message)
```
---
# Test de code source TeX
```latex
\documentclass[a4paper, 11pt, twocolumn, landscape]{report}

\usepackage[french]{babel}
\usepackage[T1]{fontenc}
\usepackage[latin1]{inputenc}

\begin{document}
	\section*{Conventions, notations et rappels}
	\addcontentsline{toc}{chapter}{Conventions}
	Au moins, c'est concis comme code.
\end{document}
```
