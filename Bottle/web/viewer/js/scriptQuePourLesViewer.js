"use strict"

//Change the is_controled state
function changeControled(){
  var sync = getSynchronize();
  setSynchronize(!sync);
  if(!sync) ws.send("GET-SLIDE");
}

function askForPoint(){
    ws.send("ASK-FOR-POINT");
    closeParameters();
}
