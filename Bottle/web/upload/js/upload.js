// Memory of the number of files originalEvent
var numberOfPresentation = 0;
var numberOfContent = 0;

$("#success").hide();
$("#fail").hide();

var fileUploadSuccess = function(data){
  $("#success").show(500);
};

var fileUploadFail = function(data){
  $("#fail").show(500);
};

var dragHandler = function(evt){
    evt.preventDefault();
};

var formData = new FormData();

// Handling drop on droparea - we laod the files in the drop area
var dropHandler = function(evt){
    evt.preventDefault();
    var element = $("#dropareaText");
    // We get the files to load
    var files = evt.originalEvent.dataTransfer.files;
    for (i=0; i<files.length; i++) {
        formData.append("presentation" + (i+numberOfPresentation).toString(), files[i]);
        element.append(files[i].name +'\n');
    }
    // updating
    numberOfPresentation += files.length;
};

// Handling drop on droparea - we laod the files in the drop area
var contentHandler = function(evt){
    evt.preventDefault();
    var element = $("#contentareaText");
    // We get the files to load
    var files = evt.originalEvent.dataTransfer.files;
    for (i=0; i<files.length; i++) {
        formData.append("content" + (i+numberOfContent).toString(), files[i]);
        element.append(files[i].name +'\n');
    }
    //Updating
    numberOfContent += files.length;
};

//Defines the actions to do on drag and drop
var dropHandlerSet = {
    dragover: dragHandler,
    drop: dropHandler
};

var contentHandlerSet = {
    dragover: dragHandler,
    drop: contentHandler
};

$("#droparea").on(dropHandlerSet);
$("#contentarea").on(contentHandlerSet);

function sendFiles(){
  var presName = $("#presnameArea").val();
  if (presName!=""){
    formData.append("name",presName);
    var req = {
        url: "/upload",
        method: "post",
        processData: false,
        contentType: false,
        data: formData
    };
    var promise = $.ajax(req);
    promise.then(fileUploadSuccess, fileUploadFail);
  }
}
