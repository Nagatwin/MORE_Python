# Web

## Les fichiers

* [index.html](Bottle/web/upload.html) : Page d'acceuil de l'interface web.


## Les dossiers

* [create](Bottle/web/create) : Les fichiers relatifs à la création de présentation.
* [css](Bottle/web/css) : Fichiers de style communs à master et viewer.
* [img](Bottle/web/img) : Fichiers images du projet.
* [js](Bottle/web/js) : Fichiers javascript communs à master et viewer, c'est le coeur du projet côté client.
* [master](Bottle/web/master) : Les fichiers relatifs à master.
* [reveal](Bottle/web/reveal) : Les fichiers relatifs à revealjs.
* [sessions](Bottle/web/sessions) : Contient un unique fichier .html gérant la liste des sessions.
* [slideshow](Bottle/web/slideshow) : Contient un unique fichier .html gérant l'affichage des slideshows.
* [upload](Bottle/web/upload) : Les fichiers relatifs à upload.
* [video](Bottle/web/video) : Contient un unique fichier .html utilisés pour les vidéos.
* [viewer](Bottle/web/viewer) : Les fichiers relatifs à viewer.