let fromServer = false;
let ws;

// More info https://github.com/hakimel/reveal.js#configuration
Reveal.initialize({
  controls: true,
  progress: true,
  history: true,
  center: true,

  transition: 'slide', // none/fade/slide/convex/concave/zoom
  
  math: {
        mathjax: 'http://cdn.mathjax.org/mathjax/latest/MathJax.js',
        config: 'TeX-AMS_HTML-full'
    }, 

  // More info https://github.com/hakimel/reveal.js#dependencies
  dependencies: [
    { src: 'lib/js/classList.js', condition: function() { return !document.body.classList; } },
    { src: 'plugin/markdown/marked.js', condition: function() { return !!document.querySelector( '[data-markdown]' ); } },
    { src: 'plugin/markdown/markdown.js', condition: function() { return !!document.querySelector( '[data-markdown]' ); } },
    { src: 'plugin/highlight/highlight.js', async: true, callback: function() { hljs.initHighlightingOnLoad(); } },
    { src: 'plugin/search/search.js', async: true },
    { src: 'plugin/zoom-js/zoom.js', async: true },
	{ src: 'plugin/math/math.js', async: true },
    //{ src: 'plugin/notes/notes.js', async: true } No speaker's view
  ]
});
Reveal.configure({
keyboard: {
    13: 'next', // go to the next slide when the ENTER key is pressed
    27: null, // do something custom when ESC is pressed
  }
});

// Some event listeners for slide changes
Reveal.addEventListener('slidechanged', send_slide);
Reveal.addEventListener('fragmentshown', send_slide);
Reveal.addEventListener('fragmenthidden', send_slide);

// Set the websocket to send back some messages
function set_websocket(w) {
    ws = w;
}

// Set the current slide
function set_slide(x, y, f) {
    fromServer = true;
    Reveal.slide(x, y, f);
    fromServer = false;
}
let isDriving = true;

function set_driving(bool){
    isDriving=bool;
}

function send_slide(event) {
    if (!fromServer) {
        const cur = Reveal.getIndices();
        if (isDriving) {
            ws.send("SET-SLIDE " + cur.h + ";" + cur.v + ";" + cur.f);
        }
    }
}