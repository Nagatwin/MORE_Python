
function changeParameters(){
  if ($("#param-col").css("display")=="none"){
    openParameters();
  }
  else{
    closeParameters();
  }
}

function openParameters(){
  $("#parameter-icon").attr('src','img/parameter-active.png');
  $("#param-col").show();
  $("iframe").css("filter","brightness(60%) blur(10px)");
  $(document).keyup(function(e) {
     if (e.keyCode == 27) {
         closeParameters();
    }
});
  $("iframe").blur();
}

function closeParameters(){
  $("#parameter-icon").attr('src','img/parameter-inactive.png');
  $("#param-col").hide();
  $("iframe").css("filter","");
  $("iframe").focus();
}

function setPos(x,y){
    $("#pointer").css("left",x);
    $("#pointer").css("top",y);
}
