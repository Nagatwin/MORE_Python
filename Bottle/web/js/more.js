let is_presentation = false;
const frame = document.getElementById('myFrame');
const checkbox = document.getElementById("drivingBox");
"use strict";
// Websocket to get content
const ws = new WebSocket("ws://"+get_ip()+"/websocket");
// Telling if the page must be synchonized
let synchronize = true;

//Getters and Setters for synchronize
function getSynchronize(){
  return synchronize;
}

function setSynchronize(bool){
  synchronize = bool;
}


let driving = true;
if (!(checkbox === null)) {
    checkbox.addEventListener('change', function () {
        if (this.checked) {
            frame.contentWindow.set_driving(true);
            driving = true;
        } else {
            frame.contentWindow.set_driving(false);
            driving = false;
        }
    });
}

// Set event handlers.
// Message
ws.onmessage = function(e) {
	console.log(e.data);
    const command = e.data.substring(0, e.data.indexOf(" "));
    let arg;
    let args;
    // Go to a slide
    if (command === "GO-SLIDE" || command === "FORCE-SLIDE" ) {
		if(driving &&
            ((synchronize || command === "FORCE-SLIDE") && typeof frame.contentWindow.set_slide === "function")) {
            arg = e.data.substring(e.data.indexOf(" ") + 1);
            args = arg.split(";");
          // Check args
            if (args.length === 3) {
              // Check fragments
                let fragment_index;
                if (args[2] === "undefined") {
                    fragment_index = undefined;
                } else {
                    fragment_index = parseInt(args[2]);
                }
                frame.contentWindow.set_slide(parseInt(args[0]), parseInt(args[1]), fragment_index);
			}
        }
        // Load a presentation
    } else if (command === "GO-TIME") {
		if (typeof frame.contentWindow.set_time === "function") {
            arg = e.data.substring(e.data.indexOf(" ") + 1);
            frame.contentWindow.set_time(parseFloat(arg));
		}
	} else if (command === "LOAD-PRES") {
        arg = e.data.substring(e.data.indexOf(" ") + 1);
		var arg2 = arg;
		if (arg.includes("?")){
			arg2=arg.substring(0,arg.indexOf("?"));
		}
		if (typeof refreshPresentationSelector !== "undefined") {
			refreshPresentationSelector(arg2);
		}
        frame.src = "Presentation/" + arg;
        is_presentation = true;
		ws.send("GET-SLIDESHOWS");
    } else if (command === "LIST-SLIDESHOWS") {
        arg = e.data.substring(e.data.indexOf(" ") + 1);
		args = arg.split(":");
		if (refreshSelectors != null) {
			refreshSelectors(args);
		}
	} else if (command == "ASKED-FOR-POINT") {
        var askingClientId = e.data.substring(e.data.indexOf(" ") + 1);
        // Shows the pop-up
        showPopUp(askingClientId);
    } else if (command == "GRANTED"){
        var clientId = e.data.substring(e.data.indexOf(" ") + 1);
        switchPopUp(clientId);
    } else if (command == "DENIED"){
        closePopUp();
    } else if (command === "GO-POINTER"){
		if (driving){
			arg = e.data.substring(e.data.indexOf(" ") + 1);
			args=arg.split(";");
			/*slides = frame.contentWindow.document.getElementById("slides");

			console.log(slides.offsetLeft);
			console.log(slides.offsetTop);
			console.log(slides.offsetLeft-slides.clientWidth/2);
			console.log(slides.offsetTop-slides.clientHeight/2);
			//*/ //This is the element that should be x0, y0
			img = document.getElementById('pointer');
			setPos(document.body.clientWidth*parseFloat(args[0])-img.clientWidth/2,document.body.clientHeight*parseFloat(args[1])-img.clientHeight/2);
		}
	} else {
        console.log("Unknown command " + command);
    }
};

const cumulativeOffset = function(element) {
    var top = 0, left = 0;
    do {
        top += element.offsetTop  || 0;
        left += element.offsetLeft || 0;
        element = element.offsetParent;
    } while(element);
    return {
        top: top,
        left: left
    };
};


// Open
ws.onopen = function(event) {
    ws.send("REGISTER " + get_hash());
    ws.send("GET-PRES");
};

// Called when presentation is loaded
function trigger() {
    frame.focus();
    if (is_presentation) {
        frame.contentWindow.set_websocket(ws);
		frame.contentWindow.document.onclick = function(e){
			if (e.which == 1 && driving) {
				ws.send("SET-POINTER " + (e.pageX/document.body.clientWidth)+";"+ (e.pageY/document.body.clientHeight));
			}
		};
        ws.send("GET-SLIDE");
    }
}


frame.onload = trigger;
