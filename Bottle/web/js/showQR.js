function showQRCode(name, URL){
    $("#popup").css("display","block");
    $("#background").css("display","block");
    $("#presName").text(name);
    new QRCode(document.getElementById("qrcode"), URL);
}

function closeQR(){
    $("#popup").css("display","none");
    $("#background").css("display","none");
    $("#qrcode").html("");
}
