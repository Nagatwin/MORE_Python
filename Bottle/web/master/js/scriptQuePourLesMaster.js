//Function for master's view
	function changepres(thelist)
	{
	  var idx = thelist.selectedIndex;
	  var content = thelist.options[idx].innerHTML;
      ws.send("SET-PRES "+content);
	}
	var selectedcurrent = null;
	function changecurrent(thelist)
	{
	  var idx = thelist.selectedIndex;
	  var content = thelist.options[idx].innerHTML;
	  var pres = document.getElementById('presid');
	var presname=pres.options[pres.selectedIndex].innerHTML;
        frame.src = "Presentation/" + presname+"?subpres="+content;
		selectedcurrent=content;
        is_presentation = true;
	}

	function changeviewerdiplay(thelist)
	{
	  var idx = thelist.selectedIndex;
	  var content = thelist.options[idx].innerHTML;
      ws.send("SET-DISPLAY "+content);
	}

	function refreshSelectors(args){
		var currentselect = document.getElementById('currentid');
		var viewerselect= document.getElementById('viewerid');


			//Updating selector of master's view
		while (currentselect.length != 0){
			currentselect.remove(0);
		}
		while (viewerselect.length != 0){
			viewerselect.remove(0);
		}
		for (var i = 0; i < args.length; i++){
		var c = document.createElement("option");
		c.text = args[i].charAt(0)=="#"?args[i].substring(1):args[i];
			viewerselect.add(c);
			if (args[i].charAt(0)=="#"){
				viewerselect.selectedIndex=i;
			}
			c = document.createElement("option");
c.text = args[i].charAt(0)=="#"?args[i].substring(1):args[i];
			currentselect.add(c);
			if ((selectedcurrent==null&&args[i].charAt(0)=="#")||selectedcurrent==args[i]){
				currentselect.selectedIndex=i;
			}
		}
	}

	function refreshPresentationSelector(arg){
		selectedcurrent = null;
		var presselect=document.getElementById('presid')
		if (presselect != null){
			var i = 0;
			while (presselect.item(i) != null){
				if (presselect.item(i).text==arg){
					presselect.selectedIndex=i;
				}
				i++;
			}
		}
	}


	// Called when someone asks for a pointer.
	function showPopUp(clientId){
		document.getElementById('askingClient').innerHTML="Client " + clientId +" asked for pointer.";
	    document.getElementById('popup').style.visibility="visible";
		document.getElementById('popup').style.opacity="1";
		//Modify the allow button
		document.getElementById("allow-btn").innerHTML = "Allow";
		document.getElementById("allow-btn").setAttribute( "onClick", "allowClient(" + clientId + ")");
		document.getElementById("deny-btn").style.display="block";
	}

	function denyClient(){
		ws.send("DENIED-POINTING");
	}
	function closePopUp(){
		document.getElementById('popup').style.opacity="0";
		document.getElementById('popup').style.visibility="hidden";
	}

	function allowClient(clientId){
		ws.send("GRANTED-POINTING "+clientId);
	}

	function switchPopUp(clientId){
		document.getElementById('askingClient').innerHTML = "Client " + clientId + " is now pointing.";
		//Modify the allow button
		document.getElementById("allow-btn").innerHTML = "Disallow";
		document.getElementById("allow-btn").setAttribute("onClick", "disallowClient(" + clientId + ")");
		document.getElementById("deny-btn").style.display="none";
	}

	function disallowClient(clientId){
		ws.send("STOP-POINTING " + clientId);
	}
