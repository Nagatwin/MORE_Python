import os
import io


class Session:
    """One set of users synchronized."""
    def __init__(self, master_uri, viewer_uri, name):
        self.name = name
        self.viewers = []
        self.masters = []
        self.presentations = []
        self.displayed = None
        self.masterHashes = set()
        self.viewerHashes = set()
        self.masterURI = master_uri
        self.viewerURI = viewer_uri

    def add_master(self, client):
        self.masters += [client]

    def add_viewer(self, client):
        self.viewers += [client]

    def remove_user(self, client_to_remove):
        self.masters = [client for client in self.masters if client != client_to_remove]
        self.viewers = [client for client in self.viewers if client != client_to_remove]

    def broadcast(self, message):
        self.send_viewers(message)
        self.send_masters(message)

    def send_viewers(self, message):
        for user in self.viewers:
            user.send(message)

    def send_masters(self, message):
        for user in self.masters:
            user.send(message)

    def upgrade_user(self, client):
        self.viewers.remove(client)
        self.masters += [client]

    def add_presentation(self, presentation):
        self.presentations += [presentation]

    def load(self):
        self.displayed = self.presentations[0]
        for pres in self.presentations:
            pres.load()


class Client:
    """Class for users."""
    identifier = 0

    def __init__(self, session_id, socket):
        """Called at the creation of an object Client.

        :param session_id: the id of the session the client belongs to.
        :param socket: the socket by which the client managed to connect to the server.
        """
        self.session = session_id
        self.socket = socket
        self.id = Client.identifier
        Client.identifier += 1
        self.__can_point = False

    def send(self, message):
        """Send a message to the client.

        :param message: the message we want to send.
        """
        self.socket.send(message)

    @property
    def can_point(self):
        """Getter for the private attribute can_point."""
        return self.__can_point

    @can_point.setter
    def can_point(self, can_point):
        """Called whenever we change the value of the attribute can_point."""
        self.__can_point = can_point


class Presentation:
    """Class for presentation."""

    def __init__(self, name):
        """Called at the creation of a Presentation object.

        :param name: the name of the presentation.
        """
        self.name = name
        self.index = '0;0;undefined'
        self.max = '0;0;undefined'
        self.slideshows = set()
        self.video = None
        self.viewer_display = None
        self.time = 0
        self.descriptor = None

    def add_slideshow(self, slideshow):
        if self.viewer_display is None:
            self.viewer_display = slideshow
        self.slideshows.add(slideshow)

    def set_video(self, video):
        if self.viewer_display is None:
            self.viewer_display = video
        self.video = video

    def set_descriptor(self, descriptor):
        self.descriptor = descriptor

    def load(self):
        descriptor = None
        video_name = None
        pres_path = 'Presentations/' + self.name
        for file in [f for f in os.listdir(pres_path) if os.path.isfile(os.path.join(pres_path, f))]:
            filename, ext = os.path.splitext(file)
            if ext == ".more":
                self.add_slideshow(Slideshow(filename))
            else:
                if ext == ".morevideo":
                    descriptor_file = io.open(os.path.join(pres_path, file))
                    descriptor = [line[:-1] if line[-1] == '\n' else line for line in descriptor_file.readlines()]
                else:
                    video_name = filename
        if video_name is not None and descriptor is not None:
            self.set_video(Video(video_name, descriptor))


class Slideshow:
    """Class for slideshows."""

    def __init__(self, name):
        self.name = name


class Video:
    """Class for videos."""

    def __init__(self, name, descriptor):
        self.name = name
        self.descriptor = descriptor

    def slide_from_time(self, time_wanted):
        slide = "0;0" #Default slide to prevent issues
        for line in self.descriptor:
            line = line.split(" ")
            time = float(line[0])
            if time <= float(time_wanted):
                slide = line[1]
        return slide

    def time_from_slide(self, slide_wanted):
        slide_wanted = ';'.join(slide_wanted.split(";")[:-1])
        time = None
        for line in self.descriptor:
            line = line.split(" ")
            if slide_wanted == line[1]:
                time = float(line[0])
        return time
