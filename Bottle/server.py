# -*-coding:Latin-1 -*
from bottle import get, run, template, route, static_file, request, post
from bottle.ext.websocket import GeventWebSocketServer
from bottle.ext.websocket import websocket
import os
import datetime
import socket
import hashlib
import io

from converter import *
from indexes import *
from moreStruct import Presentation, Slideshow, Video, Session, Client

ip = "0.0.0.0"
external_ip = "vps.nagatwin.ovh"
port = 8080
sessions = set()

# only 3 supported video formats according to https://www.w3schools.com/tags/tag_video.asp
video_file_extensions = ('.ogg', '.webm', '.mp4' )
def is_video(ext):
    return ext.lower() in video_file_extensions

# Get index
@get('/')
def index():
    template_file = io.open("web/index.html", "r", encoding='utf8')
    template_html = template_file.read()
    sessions_html = ""
    sid = 0
    for session in sessions:
        sid += 1
        sessions_html += '<tr> <td> '+str(sid)+'</td><td>' + session.name+'</td><td> <a href="' + session.masterURI +\
                         '" target="_blank"> Access </a> </td><td> <a href="' +\
                         session.viewerURI +\
                         '" target="_blank"> Access </a></td> <td> <i class="material-icons"' \
                         ' style="cursor: pointer" onclick="showQRCode(\'' + session.name +\
                         '\',\'http://\' + window.location.host + \'/\' +\'' + session.viewerURI +\
                         '\')">center_focus_weak</i></td> </tr>'
    if sessions_html == "":
        sessions_html = "<tr> <td> No active session </td><td></td><td></td><td></td></tr>"
    return template_html.replace("CONTENTTOLOAD", io.open(
        "web/sessions/sessionList.html", "r", encoding='utf8').read()).replace("SESSIONLIST", sessions_html)


# To get a presentation

@get('/Presentation/<presname>')
def presentation(presname):
    slideshow = request.query.subpres
    file_path = "Presentations/" + presname + "/" + slideshow
    # Presentation
    if os.path.isfile(file_path + ".more"):
        template_file = io.open("web/slideshow/template.html", "r", encoding='utf8')
        template_html = template_file.read()
        file = io.open(file_path + ".more", "r", encoding='utf8')
        result = file.read()
        if os.path.isfile("Presentations/" + presname + "/contents/theme.css"):
            return template_html.replace("PRESENTATION", result).replace("THEMECSS", presname + "/contents/theme.css")
        else:
            return template_html.replace("PRESENTATION", result).replace("THEMECSS", "css/theme/black.css")
    # Video
    else:
        for file in next(os.walk("./Presentations/" + presname))[2]:
            filename, ext = os.path.splitext(file)
            if filename == slideshow:
                template_file = io.open("web/video/index.html", "r", encoding='utf8')
                template_html = template_file.read()
                return template_html.replace("VIDEOSRC", "./" + presname + "/" + file).replace("VIDEOEXT",ext[1:].lower())
        print("Presentation not found")
        return


@get('/create')
def create():
    template_file = io.open("web/index.html", "r", encoding='utf8')
    template_html = template_file.read()
    box_html = ''
    for pres_name in next(os.walk('./Presentations'))[1]:
        box_html += '<div> <input type="checkbox" id="' + pres_name + '_id" name="' + pres_name +\
                    '">    <label for="' + pres_name + '_id">' + pres_name + '</label>  </div>'
    return template_html.replace("CONTENTTOLOAD",
                                 io.open("web/create/createTemplate.html", "r", encoding='utf8').read()).replace(
        "PRESENTATIONLIST", box_html)


@route('/create', method='POST')
def do_login():
    # default Name
    presname = "Presentation"
    # Checking if request is fine
    if len(request.forms) <= 1:
        return "Bad request"
    for pres in request.forms:
        if pres == "presName4POST":
            presname = request.forms.get(pres)
        elif pres not in next(os.walk('./Presentations'))[1]:
            return "Bad request"
    # Building session
    master_uri = str(hashlib.sha224(str(datetime.datetime.now().microsecond).encode('utf-8')).hexdigest())
    # Force different hash
    viewer_uri = str(hashlib.sha224(str(datetime.datetime.now().microsecond + 1).encode('utf-8')).hexdigest())
    # Getting the Name
    session = Session(master_uri, viewer_uri, presname)
    for pres in request.forms:
        if pres != "presName4POST":
            session.add_presentation(Presentation(pres))
    session.load()
    sessions.add(session)
    template_file = io.open("web/index.html", "r", encoding='utf8')
    template_html = template_file.read()
    return template_html.replace("CONTENTTOLOAD",
                                 io.open("web/create/sessionTemplate.html", "r", encoding='utf8').read()).replace(
        "MASTERURI", master_uri).replace("VIEWERURI", viewer_uri)


@get('/upload')
def upload():
    template_file = io.open("web/index.html", "r", encoding='utf8')
    template_html = template_file.read()
    return template_html.replace("CONTENTTOLOAD", io.open("web/upload/index.html", "r", encoding='utf8').read())


@post('/upload')
def upload_files():
    # Searching for the presentation name
    presname = request.forms.get('name').replace(' ','_')
    # Defines the origin path
    save_path = "Presentations/" + presname
    # Creates the folder if they don't exist
    if not os.path.exists(save_path):
        os.makedirs(os.path.normpath(save_path))
    if not os.path.exists(save_path + "/contents"):
        os.makedirs(os.path.normpath(save_path + "/contents"))
    # Searching for the content files
    content_keys = []
    for key in request.files:
        if key[:7] == "content":
            content_keys += [key]
    # Deletes the old content files
    if os.path.exists(save_path + "/contents/theme.css"):
        os.remove( save_path + "/contents/theme.css")
    for content_key in content_keys:
        filename, ext = os.path.splitext(request.files[content_key].filename)
        content_path = save_path + "/contents/" + filename + ext
        if os.path.exists(content_path):
            os.remove(content_path)
        if ext == ".css":
            request.files[content_key].save(save_path + "/contents/theme.css")
    # Searching for the files dropped in the presentation area
    pres_keys = []
    for key in request.files:
        if key[:12] == "presentation":
            pres_keys += [key]
    # A Loop on those Files
    for file_key in pres_keys:
        name, ext = os.path.splitext(request.files[file_key].filename)
        if is_video(ext) or ext == ".morevideo":
            file_path = save_path + "/" + name + ext
            # Checks if the file is already defined
            if os.path.exists(file_path):
                os.remove(file_path)
            request.files[file_key].save(file_path)
        else:
            # Checks if the file is already defined
            if os.path.exists(save_path + "/" + name + ".more"):
                os.remove(save_path + "/" + name + ".more")
            # Then saves again the new file
            request.files[file_key].save(save_path + "/" + name + ext)
            # Creates a temporary more file which will be modified (links)
            temp_more = convert_from_path(save_path + "/" + name + ext)
            # We can remove the initial file
            os.remove(save_path + "/" + name + ext)
            # Start a writer for the more file
            write_on_more = io.open(save_path + "/" + name + ".more", "a", encoding="utf8")
            # Checks if there is a content file to edit
            for line in temp_more:
                to_write = line
                for content_key in content_keys:
                    filename, ext = os.path.splitext(request.files[content_key].filename)
                    file_path = save_path + "/contents/" + filename + ext
                    if filename + ext in line:
                        if not os.path.exists(file_path):
                            request.files[content_key].save(file_path)
                        to_write = to_write.replace(filename + ext, presname + "/contents/" + filename + ext)
                write_on_more.write(to_write)
            write_on_more.close()
    return "File successfully saved"


@route('/Presentation/<filepath:path>')
def server_static(filepath):
    # Reveal files
    if ".html" in filepath:
        return '<script type="text/javascript">window.location.href ="./";</script>'
    elif os.path.isfile('web/reveal/' + filepath):
        return static_file(filepath, root='./web/reveal/')
    elif os.path.isfile('Presentations/' + filepath):
        return static_file(filepath, root='./Presentations/')
    # More files
    else:
        return static_file(filepath, root='./web/')


@route('/<filepath:path>')
def server_static(filepath):
    found_session = False
    for session in sessions:
        if filepath == session.masterURI:
            # Generate master hash for auth
            master_hash = str(hashlib.sha224(str(datetime.datetime.now().microsecond).encode('utf-8')).hexdigest())
            session.masterHashes.add(master_hash)
            template_file = io.open("web/master/index.html", "r", encoding='utf8')
            template_html = template_file.read()
            presentation_html = ""
            for pres in session.presentations:
                presentation_html += "<option>" + pres.name + "</option>"
            return template_html.replace('MASTER_HASH', "'" + master_hash + "'").replace('PRESENTATIONLIST',
                                                                                         presentation_html).replace(
                'SERVER_IP', "'" + external_ip + ":"+str(port) + "'").replace("WINTITLE", "MORE - "+session.name + " - master")
        elif filepath == session.viewerURI:
            viewer_hash = str(hashlib.sha224(str(datetime.datetime.now().microsecond).encode('utf-8')).hexdigest())
            session.viewerHashes.add(viewer_hash)
            template_file = io.open("web/viewer/index.html", "r", encoding='utf8')
            template_html = template_file.read()
            return template_html.replace('VIEWER_HASH', "'" + viewer_hash + "'").replace('SERVER_IP', "'" + external_ip + ":" +
                                                                                         str(port) + "'").replace("WINTITLE", "MORE - "+session.name + " - viewer")
    if not found_session:
        if ".html" in filepath:
            return '<script type="text/javascript">window.location.href ="./";</script>'
        else:
            return static_file(filepath, root='web/')


@get('/websocket', apply=[websocket])
def echo(ws):
    global sessions
    client = Client(None, ws)
    # Handle messages
    while True:
        msg = ws.receive()
        print(msg)
        if msg is not None:
            args = msg.split()
            # Setting the index of the presentation
            if client.session is not None:
                if args[0] == 'SET-SLIDE':
                    if len(args) == 2:
                        if client in client.session.masters:
                            client.session.displayed.index = args[1]
                            if client.session.displayed.video is not None:
                                if client.session.displayed.video.time_from_slide(args[1]) is not None:
                                    client.session.displayed.time = client.session.displayed.video.time_from_slide(args[1])
                                    client.session.broadcast(
                                        'GO-TIME ' + str(client.session.displayed.video.time_from_slide(args[1])))
                            # Updating the new max index
                            session.displayed.max = get_max_index(session.displayed.max, args[1])
                            client.session.broadcast('GO-SLIDE ' + args[1])
                            print('Index synchronized to : ' + args[1])
                        # If it's a viewer, we make sure that he does not go further than the master
                        else:
                            if get_max_index(client.session.displayed.max, args[1]) == args[1]:
                                client.send('FORCE-SLIDE ' + client.session.displayed.max)
                            else:
                                client.send('FORCE-SLIDE ' + args[1])
                    else:
                        print('Invalid request : ' + msg)
                # Sync video
                elif args[0] == 'SET-TIME':
                    if client in client.session.masters:
                        client.session.displayed.index = client.session.displayed.video.slide_from_time(
                            float(args[1])) + ";undefined"
                        client.session.displayed.max = get_max_index(session.displayed.max,
                                                                     client.session.displayed.video.slide_from_time(
                                                                         float(args[1])) + ";undefined")
                        client.session.displayed.time = float(args[1])
                        if client.session.displayed.viewer_display == client.session.displayed.video:
                            client.session.send_viewers('GO-TIME ' + args[1])
                            print('Video synchronized')
                        else:
                            client.session.broadcast('GO-SLIDE ' + str(client.session.displayed.video.slide_from_time(
                                float(args[1])) + ";undefined"))
                            print('Slide synchronized')
                # Setting the presentation
                elif args[0] == 'SET-PRES':
                    if len(args) != 1:
                        presname = ' '.join(args[1:])
                        if client in client.session.masters:
                            for pres in client.session.presentations:
                                if pres.name == presname:
                                    client.session.displayed = pres
                                    client.session.broadcast(
                                        'LOAD-PRES ' + pres.name + "?subpres=" +
                                        pres.viewer_display.name)
                                    print('Presentation synchronized')
                    else:
                        print('Invalid request : ' + msg)
                # Setting the slideshow for viewers
                elif args[0] == 'SET-DISPLAY':
                    if len(args) != 1:
                        displayname = ' '.join(args[1:])
                        if client in client.session.masters:
                            for slideshow in client.session.displayed.slideshows:
                                if slideshow.name == displayname:
                                    client.session.displayed.viewer_display = slideshow
                                    client.session.send_viewers(
                                        'LOAD-PRES ' + client.session.displayed.name + "?subpres=" + slideshow.name)
                                    print('Slideshow synchronized')
                            if client.session.displayed.video is not None and client.session.displayed.video.name == \
                                    displayname:
                                client.session.displayed.viewer_display = client.session.displayed.video
                                client.session.send_viewers(
                                    'LOAD-PRES ' + client.session.displayed.name + "?subpres=" +
                                    client.session.displayed.video.name)
                                print('Slideshow synchronized')
                    else:
                        print('Invalid request : ' + msg)
                # Get current slideshow
                elif args[0] == 'GET-PRES':
                    if len(args) == 1:
                        client.send(
                            'LOAD-PRES ' + client.session.displayed.name + "?subpres=" +
                            client.session.displayed.viewer_display.name)
                        print('Current slideshow given')
                    else:
                        print('Invalid request : ' + msg)
                # Get the list of Slideshow
                elif args[0] == 'GET-SLIDESHOWS':
                    if len(args) == 1:
                        if client in client.session.masters:
                            slideshows_names = [("#" if x == session.displayed.viewer_display else "") + x.name for x in
                                                client.session.displayed.slideshows]
                            if client.session.displayed.video is not None:
                                video_name = [
                                    (
                                            ("#" if client.session.displayed.video == session.displayed.viewer_display
                                             else "") + client.session.displayed.video.name
                                    )
                                ]
                            else:
                                video_name = []
                            client.send('LIST-SLIDESHOWS ' + ':'.join(slideshows_names + video_name))
                        print('Current slideshow list given')
                    else:
                        print('Invalid request : ' + msg)
                # Get current index
                elif args[0] == 'GET-SLIDE':
                    if len(args) == 1:
                        client.send('GO-SLIDE ' + client.session.displayed.index)
                        client.send("GO-TIME " + str(client.session.displayed.time))
                        print('Current slide given')
                    else:
                        print('Invalid request : ' + msg)
                elif args[0] == 'ASK-FOR-POINT':
                    if len(args) == 1:
                        clients_can_point = [c for c in client.session.viewers if c.can_point]
                        if len(clients_can_point)==0 :
                            client.session.send_masters('ASKED-FOR-POINT ' + str(client.id))
                            print('Query for pointing by ' + str(client.id) + 'transmitted to masters')
                    else:
                        print('Invalid request : ' + msg)

                elif args[0] == 'DENIED-POINTING' :
                    if len(args) == 1:
                        client.session.send_masters('DENIED ');
                    else:
                        print('Invalid request : ' + msg)

                elif args[0] == 'GRANTED-POINTING':
                    if len(args) == 2:
                        client_granted = [c for c in client.session.viewers if c.id == int(args[1])]
                        if len(client_granted) == 0:
                            print('No client found for the id : ' + args[1])
                        else:
                            client_granted[0].can_point = True
                        # We tell the other masters that it is GRANTED
                        client.session.send_masters('GRANTED ' + str(client_granted[0].id))
                    else:
                        print('Invalid request : ' + msg)
                elif args[0] == 'STOP-POINTING':
                    if len(args) == 2:
                        client_stopped = [c for c in client.session.viewers if c.id == int(args[1])]
                        if len(client_stopped) == 0:
                            print('No client found for the id : ' + args[1])
                        else:
                            client_stopped[0].can_point = False
                            client.session.send_masters('DENIED ')
                    else:
                        print('Invalid request : ' + msg)
                elif args[0] == 'SET-POINTER':
                    if len(args) == 2:
                        if client.can_point:
                            client.session.broadcast("GO-POINTER " + args[1])
                    else:
                        print('Invalid request : ' + msg)
            # Upgrading rights
            elif args[0] == 'REGISTER':
                if len(args) == 2:
                    for session in sessions:
                        if args[1] in session.masterHashes:
                            client = Client(session, ws)
                            client.can_point = True
                            session.masterHashes.remove(args[1])
                            session.add_master(client)
                            print('Master added')
                        elif args[1] in session.viewerHashes:
                            client = Client(session, ws)
                            session.viewerHashes.remove(args[1])
                            session.add_viewer(client)
                            print('Viewer added')
                else:
                    print('Invalid request : ' + msg)
            else:
                print('Invalid command : ' + args[0])
        else:
            break
    for session in sessions:
        session.remove_user(client)
    print("Client left")
    new_sessions = set()
    for session in sessions:
        if len(session.masters)+len(session.viewers) != 0:
            new_sessions.add(session)
        else:
            print("Session ended")
    sessions = new_sessions


run(host=ip, port=port, server=GeventWebSocketServer)
