# Bottle Websocket Client/Serveur

Utilisation d'un serveur HTTP et Websocket avec la librairie bottle.


## Le fonctionnement général

### Le lancement du serveur

Le serveur crée les divers serveurs webs et accès.

### Le client peut se connecter via les urls générées par le serveur web

1. Les fichiers sont donnés par le serveur ;
2. Le client demande la présentation actuelle et la charge dans l'iframe ;
3. Le client demande l'index actuel et se positionne dessus ;
4. Le cient écoute alors les changements de slides et les effectue si il est synchronisé ;
5. Les masters envoient au serveurs les changements de slide et de prsentation qui sont broadcast aux autres users.

### La page d'upload

* on glisse les fichiers .md d'un côté et les fichiers de contenus de l'autre, sans distinction des présentations ;
* le serveur gère la conversion automatiquement.


## Les fichiers
### [server.py](Bottle/server.py) : Le fichier du serveur

* démarre un serveur web sur localhost:8080 ;
* démarre un serveur websocket sur localhost:8080/websocket ;
* démarre un serveur d'upload sur localhost:8080/upload ;
* sert une session seule accessible via des urls générées par le serveur web ;
* gère les commandes envoyées par les différents clients.

### [converter.py](Bottle/converter.py) : Le fichier de conversion

* convertit un fichier .md en fichier .more ;
* définition de fonctions auxiliaires utiles.

### [client.py](Bottle/client.py) : La classe client

### [moreStruct.py](Bottle/moreStruct.py) : La librairie avec les classes de base

#### Slideshow

* définit la classe Slideshow.

#### Video

* définit la classe vidéo

#### Session

* définit les hashs ;
* définit les URI.

#### Client

* associe une session à un client ;
* associe une socket à un client ;
* attribue un id unique ;
* définit le droit d'afficher un pointeur.

### [run.bat](Bottle/run.bat) : Le runner

Lanceur pour le serveur sous windows (suppose que python est dans path).

## Les dossiers
### [web](Bottle/web) : html/js/css

Dossier qui gère les fichiers html/js/css envoyés au client.

### [Presentations](Bottle/Presentations) : Les présentations

Les présentations qui ont été mises sur le serveur.

### [Tests](Bottle/Tests) : Les présentations de test

Les présentation de test fournies


## Interface Client serveur

Le set de commandes échangées entre le client et le serveur est résumé dans le tableau ci-dessous :

| Commande | Args | De | À | Effet |
| ------------- |:-------------:|:----:|:----:|:----:|
| REGISTER | "hash" | Client | Serveur | Demande au serveur de créer une session adaptée au hash donné. |
| GET-SLIDE | - | Client | Serveur | Demande au serveur la slide actuelle. |
| SET-SLIDE | "x;y;f" | Client | Serveur | Dit au serveur de définir la slide actuelle sur x;y au fragment f. Ne marche que si le client est master. |
| GO-SLIDE | "x;y;f" | Serveur | Client | Dit au client de se synchroniser sur la slide x;y au fragment f. |
| FORCE-SLIDE | "x;y;f" | Serveur | Client | Dit au client de se synchroniser sur la slide x;y au fragment f, même si celui-ci est désynchronisé. |
| SET-TIME | t | Client | Serveur | Dit au serveur de définir le temps actuel de la vidéo à t secondes. Ne marche que si le client est master. |
| GO-TIME | t | Serveur | Client | Dit au client de se synchroniser sur le temps t. |
| UPGRADE | "hash" | Client | Serveur | Demande au serveur d'upgrade à master. |
| GET-PRES | - | Client | Serveur | Demande au serveur de donner la présentation courante. |
| SET-PRES | "pres name" | Client | Serveur | Dit au serveur de définir la présentation actuelle. |
| LOAD-PRES | "pres name" | Serveur | Client | Dit au client de changer de présentation. |
| ASK-FOR-POINT | - | Client | Serveur | Dit au serveur que le client souhaite pouvoir afficher son pointeur. |
| ASKED-FOR-POINT | "client id" | Serveur | Client | Dit aux masters que le client d'id "client id" souhaite afficher son pointeur. |
| GRANTED-POINTING | "client id" | Client | Serveur | Dit au serveur que le client d'id "client id" est autorisé à afficher son pointeur. |
| GRANTED | - | Serveur | Client | Dit au masters que la requete a été acceptée. |
| DENIED-POINTING | "client id" | Client | Serveur | Dit au serveur que le client d'id "client id" est autorisé à afficher son pointeur. |
| DENIED | - | Serveur | Client | Dit au masters que la requete a été refusée. |
| STOP-POINTING | "client id" | Client | Serveur | Dit au serveur d'arrêter de donner le droit de pointer au client d'id donné. |
| SET-POINTER | "x;y" | Client | Serveur | Donne au serveur la position actuelle du pointeur affiché. |
| GO-POINTER | "x;y" | Serveur | Client | Donne aux clients la position actuelle du pointeur affiché. |
| GET-SLIDESHOWS | - | Client | Serveur | Dit au serveur que le client veut récupérer toutes les slideshows de la présentation dans laquelle il se trouve. |
| LIST-SLIDESHOWS | "list-slideshows" | Serveur | Client | Donne au client la liste des slideshows de la présentation dans laquelle il se trouve. |