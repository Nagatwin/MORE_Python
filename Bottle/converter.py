#!/usr/bin/env python
# -*- coding: utf-8 -*-
import io
import os


def convert_from_path(file_path):
    """Convert a file from his path to a .more one.

    :param file_path: a string containing the path of the file that has to be converted.
    :return: a string containing the html code.
    """
    temp_path = "/".join(file_path.split("/")[:-1]) + "/temp.more"
    os.system("pandoc -t revealjs -o " + temp_path + " " + file_path +
              " -V revealjs-url=http://lab.hakim.se/reveal-js --mathjax --no-highlight --variable hlss=zenburn")
    temp = io.open(temp_path, "r", encoding="utf8")
    res = temp.readlines()
    temp.close()
    os.remove(temp_path)
    return res


# Redefinition of the string to int cast
def to_int(element):
    if element == "undefined":
        return -2
    else:
        return int(element)
