FROM python:3.10-slim

WORKDIR /usr/src/app

RUN apt-get update \
    && apt-get install -y --no-install-recommends \
        pandoc \
    && apt-get clean

RUN python -m pip install --no-cache-dir --upgrade pip && \
    python -m pip install --no-cache-dir bottle bottle-websocket

COPY ./Bottle .

CMD ["python", "./server.py"]
